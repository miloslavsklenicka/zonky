package cz.itify.zonky.domain;

import java.util.Date;

public class InsuranceHistory {
    private Date policyPeriodFrom;
    private Date policyPeriodTo;

    public Date getPolicyPeriodFrom() {
        return policyPeriodFrom;
    }
    public void setPolicyPeriodFrom(Date policyPeriodFrom) {
        this.policyPeriodFrom = policyPeriodFrom;
    }
    public Date getPolicyPeriodTo() {
        return policyPeriodTo;
    }
    public void setPolicyPeriodTo(Date policyPeriodTo) {
        this.policyPeriodTo = policyPeriodTo;
    }

    @Override
    public String toString() {
        return "InsuranceHistory [policyPeriodFrom=" + policyPeriodFrom + ", policyPeriodTo=" + policyPeriodTo + "]";
    }
}
