package cz.itify.zonky.domain;

import java.time.OffsetDateTime;
import java.util.List;

public class Loan {
    private Long id;
    private String url;
    private String name;
    private String story;
    private int purpose;
    private List<Photo> photos;
    private Long userId;
    private String nickName;
    private int termInMonths;
    private float interestRate;
    private float revenueRate;
    private float annuity;
    private int premium;
    private String rating;
    private boolean topped;
    private float amount;
    private float remainingInvestment;
    private float investmentRate;
    private boolean covered;
    private float reservedAmount;
    private float zonkyPlusAmount;
    private OffsetDateTime datePublished;
    private boolean published;
    private OffsetDateTime deadline;
    private String myOtherInvestments;
    private String borrowerRelatedInvestmentInfo;
    private int investmentsCount;
    private int questionsCount;
    private int region;
    private String mainIncomeType;
    private boolean questionsAllowed;
    private String activeLoansCount;
    private boolean insuranceActive;
    private List<InsuranceHistory> insuranceHistory;  
    private boolean fastcash;
    private boolean multicash;
    private float annuityWithInsurance;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStory() {
        return story;
    }
    public void setStory(String story) {
        this.story = story;
    }
    public int getPurpose() {
        return purpose;
    }
    public void setPurpose(int purpose) {
        this.purpose = purpose;
    }
    public List<Photo> getPhotos() {
        return photos;
    }
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public int getTermInMonths() {
        return termInMonths;
    }
    public void setTermInMonths(int termInMonths) {
        this.termInMonths = termInMonths;
    }
    public float getInterestRate() {
        return interestRate;
    }
    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }
    public float getRevenueRate() {
        return revenueRate;
    }
    public void setRevenueRate(float revenueRate) {
        this.revenueRate = revenueRate;
    }
    public float getAnnuity() {
        return annuity;
    }
    public void setAnnuity(float annuity) {
        this.annuity = annuity;
    }
    public int getPremium() {
        return premium;
    }
    public void setPremium(int premium) {
        this.premium = premium;
    }
    public String getRating() {
        return rating;
    }
    public void setRating(String rating) {
        this.rating = rating;
    }
    public boolean isTopped() {
        return topped;
    }
    public void setTopped(boolean topped) {
        this.topped = topped;
    }
    public float getAmount() {
        return amount;
    }
    public void setAmount(float amount) {
        this.amount = amount;
    }
    public float getRemainingInvestment() {
        return remainingInvestment;
    }
    public void setRemainingInvestment(float remainingInvestment) {
        this.remainingInvestment = remainingInvestment;
    }
    public float getInvestmentRate() {
        return investmentRate;
    }
    public void setInvestmentRate(float investmentRate) {
        this.investmentRate = investmentRate;
    }
    public boolean isCovered() {
        return covered;
    }
    public void setCovered(boolean covered) {
        this.covered = covered;
    }
    public float getReservedAmount() {
        return reservedAmount;
    }
    public void setReservedAmount(float reservedAmount) {
        this.reservedAmount = reservedAmount;
    }
    public float getZonkyPlusAmount() {
        return zonkyPlusAmount;
    }
    public void setZonkyPlusAmount(float zonkyPlusAmount) {
        this.zonkyPlusAmount = zonkyPlusAmount;
    }
    public OffsetDateTime getDatePublished() {
        return datePublished;
    }
    public void setDatePublished(OffsetDateTime datePublished) {
        this.datePublished = datePublished;
    }
    public boolean isPublished() {
        return published;
    }
    public void setPublished(boolean published) {
        this.published = published;
    }
    public OffsetDateTime getDeadline() {
        return deadline;
    }
    public void setDeadline(OffsetDateTime deadline) {
        this.deadline = deadline;
    }
    public String getMyOtherInvestments() {
        return myOtherInvestments;
    }
    public void setMyOtherInvestments(String myOtherInvestments) {
        this.myOtherInvestments = myOtherInvestments;
    }
    public String getBorrowerRelatedInvestmentInfo() {
        return borrowerRelatedInvestmentInfo;
    }
    public void setBorrowerRelatedInvestmentInfo(String borrowerRelatedInvestmentInfo) {
        this.borrowerRelatedInvestmentInfo = borrowerRelatedInvestmentInfo;
    }
    public int getInvestmentsCount() {
        return investmentsCount;
    }
    public void setInvestmentsCount(int investmentsCount) {
        this.investmentsCount = investmentsCount;
    }
    public int getQuestionsCount() {
        return questionsCount;
    }
    public void setQuestionsCount(int questionsCount) {
        this.questionsCount = questionsCount;
    }
    public int getRegion() {
        return region;
    }
    public void setRegion(int region) {
        this.region = region;
    }
    public String getMainIncomeType() {
        return mainIncomeType;
    }
    public void setMainIncomeType(String mainIncomeType) {
        this.mainIncomeType = mainIncomeType;
    }
    public boolean isQuestionsAllowed() {
        return questionsAllowed;
    }
    public void setQuestionsAllowed(boolean questionsAllowed) {
        this.questionsAllowed = questionsAllowed;
    }
    public String getActiveLoansCount() {
        return activeLoansCount;
    }
    public void setActiveLoansCount(String activeLoansCount) {
        this.activeLoansCount = activeLoansCount;
    }
    public boolean isInsuranceActive() {
        return insuranceActive;
    }
    public void setInsuranceActive(boolean insuranceActive) {
        this.insuranceActive = insuranceActive;
    }
    public List<InsuranceHistory> getInsuranceHistory() {
        return insuranceHistory;
    }
    public void setInsuranceHistory(List<InsuranceHistory> insuranceHistory) {
        this.insuranceHistory = insuranceHistory;
    }
    public boolean isFastcash() {
        return fastcash;
    }
    public void setFastcash(boolean fastcash) {
        this.fastcash = fastcash;
    }
    public boolean isMulticash() {
        return multicash;
    }
    public void setMulticash(boolean multicash) {
        this.multicash = multicash;
    }
    public float getAnnuityWithInsurance() {
        return annuityWithInsurance;
    }
    public void setAnnuityWithInsurance(float annuityWithInsurance) {
        this.annuityWithInsurance = annuityWithInsurance;
    }

    @Override
    public String toString() {
        return "Loan [id=" + id + ", url=" + url + ", name=" + name + ", story=" + story + ", purpose=" + purpose
                + ", photos=" + photos + ", userId=" + userId + ", nickName=" + nickName + ", termInMonths="
                + termInMonths + ", interestRate=" + interestRate + ", revenueRate=" + revenueRate + ", annuity="
                + annuity + ", premium=" + premium + ", rating=" + rating + ", topped=" + topped + ", amount=" + amount
                + ", remainingInvestment=" + remainingInvestment + ", investmentRate=" + investmentRate + ", covered="
                + covered + ", reservedAmount=" + reservedAmount + ", zonkyPlusAmount=" + zonkyPlusAmount
                + ", datePublished=" + datePublished + ", published=" + published + ", deadline=" + deadline
                + ", myOtherInvestments=" + myOtherInvestments + ", borrowerRelatedInvestmentInfo="
                + borrowerRelatedInvestmentInfo + ", investmentsCount=" + investmentsCount + ", questionsCount="
                + questionsCount + ", region=" + region + ", mainIncomeType=" + mainIncomeType + ", questionsAllowed="
                + questionsAllowed + ", activeLoansCount=" + activeLoansCount + ", insuranceActive=" + insuranceActive
                + ", insuranceHistory=" + insuranceHistory + ", fastcash=" + fastcash + ", multicash=" + multicash
                + ", annuityWithInsurance=" + annuityWithInsurance + "]";
    }
}
