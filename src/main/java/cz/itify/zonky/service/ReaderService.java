package cz.itify.zonky.service;

import java.io.IOException;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.itify.zonky.domain.Loan;

@Service
public class ReaderService {

    @Value("${api.url}")
    private String apiURL;

    @Value("${api.url.loans.relative}")
    private String apiLoansURL;

    @Value("${api.loans.first.readings.history}")
    private Long firstHistoryReadings;

    @Value("${api.loans.query}")
    private String query;

    @Value("${api.loans.pageSize}")
    private String pageSize;

    @Value("${api.loans.orderedBy}")
    private String orderedBy;

    private Instant lastTime;

    @Scheduled(fixedRate=300000)
    public void scheduletask() throws IOException {
        if (this.lastTime == null) {
            getNewLoans(Instant.now().minusMillis(firstHistoryReadings));
        }
        else {
            getNewLoans(this.lastTime);
        }
    }

    private String getPrettyLoans(Loan[] loans) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(loans);
    }

    private void getNewLoans(Instant timePublishedFrom) throws IOException {
        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.set("X-Size", this.pageSize);
        reqHeaders.set("X-Order", this.orderedBy);
        HttpEntity<String> httpEntity = new HttpEntity<String>(reqHeaders);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(apiURL+apiLoansURL).queryParam(this.query, timePublishedFrom);

        System.out.println("Running request: " + uriBuilder.toUriString());

        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<Loan[]> responseEntity = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, httpEntity, Loan[].class);
        Loan[] loans = responseEntity.getBody();

        System.out.println(getPrettyLoans(loans));

        if (loans.length > 0) {
            this.lastTime = loans[0].getDatePublished().toInstant();
        }
        else {
            this.lastTime = Instant.ofEpochMilli(responseEntity.getHeaders().getDate());
        }
    }
}
